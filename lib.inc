section .text
%define SYSCALL_EXIT 60
%define SYSCALL_WRITE 1
%define SYSCALL_READ 0
%define OUT 1
%define IN 0

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:   
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, OUT
    syscall
    ret
    
    
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYSCALL_WRITE
    mov rdi,OUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
   


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    test rdi, rdi
    jge .big
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
   .big:
    jmp  print_uint
    


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rbx, rsp
    mov rax, rdi
    dec rsp
    push 0
    mov r8, 10
    .loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
	dec rsp       
	mov [rsp], dl
        test rax, rax
        jnz .loop
    .end:
	mov rdi, rsp
        call print_string
    .loopik:
	inc rsp
	cmp rbx, rsp
        jnz .loopik
	xor rbx, rbx
        xor rdx, rdx
        ret


 
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov cl, byte[rdi + rax]
        mov bl, byte[rsi + rax]
        cmp cl, bl
        je .end
        xor rax, rax
        jmp .ret
    .end:
        inc rax
        test cl,cl
        jnz .loop
        mov rax, 1
    .ret:
        xor bl, bl
        xor cl, cl
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYSCALL_READ 
    mov rdi, IN
    dec rsp
    mov rsi,rsp
    mov rdx, 1
    syscall
    test rax, rax
    je .end
    mov al, [rsp]
    .end:
        inc rsp
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rax, rax
    sub rsp, 48
    mov [rsp], rdi
    mov [rsp + 16], rsi
    xor rcx, rcx
    mov [rsp + 32], rcx
    .loop:
        call read_char
        mov rdi, [rsp]
        mov rsi, [rsp + 16]
        mov rcx, [rsp + 32]
        test rax, rax
        je .end_word
        cmp rax, 0x20
        je .probel
        cmp rax, 0x9
        je .probel
        cmp rax, 0xA
        je .probel
        cmp rcx, rsi
        je .end_buffer
        mov [rdi + rcx], rax
        inc rcx
        mov [rsp + 32], rcx
        jmp .loop
    .end_buffer:
        xor rax, rax
        jmp .end
    .probel:
        test rcx, rcx
        jz .loop
    .end_word:
        xor byte[rdi + rcx], 0
        mov rax, rdi
        mov rdx, rcx
    .end:
        add rsp, 48
        ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    mov r9, 10
    .loop:
        mov cl, byte[rdi + r8]
        sub cl, '0'
        test cl, cl
        jl .end
        cmp cl, 9
        jg .end
        mul r9
        inc r8
        add rax, rcx
        jmp .loop
   .end:
        mov rdx, r8
        xor r8, r8
        xor rcx, rcx
        xor r9, r9
        ret
   




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        mov r8, [rdi + rax]
        test rdx, rdx 
        jle .end
        dec rdx
        test r8, r8
        je .end_line
        mov [rsi + rax], r8
        inc rax
        jmp .loop
    .end:
        mov rax, rdx
    .end_line:
        xor r8, r8
        xor r9, r9
        ret
